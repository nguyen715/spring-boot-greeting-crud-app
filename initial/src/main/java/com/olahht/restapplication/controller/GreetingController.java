package com.olahht.restapplication.controller;

import com.olahht.restapplication.model.Greeting;
import com.olahht.restapplication.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/greeting")
public class GreetingController {

    @Autowired
    private GreetingService greetingService;

    @GetMapping("/random")
    public Greeting randomGreeting() {
        return greetingService.randomGreeting();
    }

    @GetMapping("/")
    public ArrayList<Greeting> getAllGreetings() {
        return greetingService.read();
    }

    @GetMapping("/{id}")
    public Greeting getGreeting(@PathVariable long id) {
        return greetingService.read(id);
    }

    @PostMapping("/")
    public Greeting addGreeting(@RequestBody String content){
        return greetingService.create(content);
    }

    @PutMapping("/{id}")
    public Greeting updateGreeting(@PathVariable long id, @RequestBody Greeting greeting) {
        return greetingService.update(id, greeting);
    }

    @DeleteMapping("/{id}")
    public void removeGreeting(@PathVariable long id) {
        greetingService.delete(id);
    }
}
