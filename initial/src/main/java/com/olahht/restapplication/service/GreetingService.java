package com.olahht.restapplication.service;

import com.olahht.restapplication.model.Greeting;
import com.olahht.restapplication.repository.GreetingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class GreetingService {

    @Autowired
    private GreetingRepository greetingRepository;
    private final static String[] randomGreetings = new String[] {"greeting1", "greeting2", "greeting3"};

    private static int getRandom() {
        return (int) (Math.random() * randomGreetings.length);
    }

    public Greeting randomGreeting() {
        int index = getRandom();
        String greetingContent = randomGreetings[index];
        return create(greetingContent);
    }

    static int getRandomGreetingCount() {
        return randomGreetings.length;
    }

    public ArrayList<Greeting> read() {
        return (ArrayList<Greeting>) greetingRepository.findAll();
    }

    public Greeting read(Long id) {
        return greetingRepository.findById(id).orElse(null);
    }

    public Greeting create(String content) {
        return greetingRepository.save(new Greeting(content));
    }

    public Greeting update(long id, Greeting newGreeting) {
        Greeting greetingInDb = greetingRepository.findById(id).orElse(null);
        if(greetingInDb == null) {
            return null;
        }
        greetingInDb.setContent(newGreeting.getContent());
        return greetingRepository.save(greetingInDb);
    }

    public void delete(long id) {
        greetingRepository.deleteById(id);
    }
}
