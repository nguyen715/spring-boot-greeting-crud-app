package com.olahht.restapplication.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="GREETING")
public class Greeting {

    @Getter @Id @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Getter @Setter @Column(name="CONTENT")
    private String content;

    public Greeting() {
    }

    public Greeting(String content) {
        this.content = content;
    }
}
