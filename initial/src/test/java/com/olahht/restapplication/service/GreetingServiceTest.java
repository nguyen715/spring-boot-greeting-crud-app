package com.olahht.restapplication.service;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GreetingServiceTest {
    @Test
    public void testRandomGreeting() {
        Set<String> myGreetings = new HashSet<>();
        GreetingService greetingService = new GreetingService();
        for(int i=0; i<10000; i++) {
            myGreetings.add(greetingService.randomGreeting().getContent());
        }
        assertEquals(GreetingService.getRandomGreetingCount(), myGreetings.size());
    }

    @Test
    public void testReadAll() {


    }
}